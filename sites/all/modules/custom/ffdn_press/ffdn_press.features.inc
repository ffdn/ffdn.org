<?php

/**
 * @file
 * ffdn_press.features.inc
 */

/**
 * Implements hook_views_api().
 */
function ffdn_press_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function ffdn_press_node_info() {
  $items = array(
    'communique_presse' => array(
      'name' => t('Communiqué de presse'),
      'base' => 'node_content',
      'description' => t('Utilisez les <em>communiqués de presse</em> pour des contenus adressés à la presse.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'press_review' => array(
      'name' => t('Revue de presse'),
      'base' => 'node_content',
      'description' => t('Utilisez les <em>Revues de presse</em> pour des contenus concernant des articles présents sur d\'autres sites, journaux, blogs…'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
