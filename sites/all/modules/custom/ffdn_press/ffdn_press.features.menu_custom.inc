<?php

/**
 * @file
 * ffdn_press.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function ffdn_press_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-suivez-nous.
  $menus['menu-suivez-nous'] = array(
    'menu_name' => 'menu-suivez-nous',
    'title' => 'Suivez-nous',
    'description' => 'Menu de liens sociaux (RSS, Twitter, whatever)',
    'language' => 'und',
    'i18n_mode' => 5,
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Menu de liens sociaux (RSS, Twitter, whatever)');
  t('Suivez-nous');

  return $menus;
}
