<?php

/**
 * @file
 * ffdn_press.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function ffdn_press_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-suivez-nous_presse:revue-de-presse/feed.xml.
  $menu_links['menu-suivez-nous_presse:revue-de-presse/feed.xml'] = array(
    'menu_name' => 'menu-suivez-nous',
    'link_path' => 'revue-de-presse/feed.xml',
    'router_path' => 'revue-de-presse/feed.xml',
    'link_title' => 'Presse',
    'options' => array(
      'attributes' => array(
        'title' => 'RSS Revue de presse',
      ),
      'alter' => TRUE,
      'identifier' => 'menu-suivez-nous_presse:revue-de-presse/feed.xml',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Presse');

  return $menu_links;
}
