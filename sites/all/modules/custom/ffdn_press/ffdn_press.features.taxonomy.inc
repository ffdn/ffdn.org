<?php

/**
 * @file
 * ffdn_press.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function ffdn_press_taxonomy_default_vocabularies() {
  return array(
    'tags_press_review' => array(
      'name' => 'Étiquettes (revue de presse)',
      'machine_name' => 'tags_press_review',
      'description' => 'Utilisez les étiquettes pour regrouper les revues de presse sur des sujets similaires dans des catégories.',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
      'language' => 'und',
      'i18n_mode' => 4,
      'base_i18n_mode' => 4,
      'base_language' => 'und',
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
  );
}
