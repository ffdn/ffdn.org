<?php

/**
 * @file
 * ffdn_press.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function ffdn_press_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create communique_presse content'.
  $permissions['create communique_presse content'] = array(
    'name' => 'create communique_presse content',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
      'contributeur' => 'contributeur',
    ),
    'module' => 'node',
  );

  // Exported permission: 'create press_review content'.
  $permissions['create press_review content'] = array(
    'name' => 'create press_review content',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
      'contributeur' => 'contributeur',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any communique_presse content'.
  $permissions['delete any communique_presse content'] = array(
    'name' => 'delete any communique_presse content',
    'roles' => array(
      'administrator' => 'administrator',
      'contributeur' => 'contributeur',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any press_review content'.
  $permissions['delete any press_review content'] = array(
    'name' => 'delete any press_review content',
    'roles' => array(
      'administrator' => 'administrator',
      'contributeur' => 'contributeur',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own communique_presse content'.
  $permissions['delete own communique_presse content'] = array(
    'name' => 'delete own communique_presse content',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own press_review content'.
  $permissions['delete own press_review content'] = array(
    'name' => 'delete own press_review content',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
      'contributeur' => 'contributeur',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete terms in tags_press_review'.
  $permissions['delete terms in tags_press_review'] = array(
    'name' => 'delete terms in tags_press_review',
    'roles' => array(
      'administrator' => 'administrator',
      'contributeur' => 'contributeur',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit any communique_presse content'.
  $permissions['edit any communique_presse content'] = array(
    'name' => 'edit any communique_presse content',
    'roles' => array(
      'administrator' => 'administrator',
      'contributeur' => 'contributeur',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any press_review content'.
  $permissions['edit any press_review content'] = array(
    'name' => 'edit any press_review content',
    'roles' => array(
      'administrator' => 'administrator',
      'contributeur' => 'contributeur',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own communique_presse content'.
  $permissions['edit own communique_presse content'] = array(
    'name' => 'edit own communique_presse content',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
      'contributeur' => 'contributeur',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own press_review content'.
  $permissions['edit own press_review content'] = array(
    'name' => 'edit own press_review content',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
      'contributeur' => 'contributeur',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit terms in tags_press_review'.
  $permissions['edit terms in tags_press_review'] = array(
    'name' => 'edit terms in tags_press_review',
    'roles' => array(
      'administrator' => 'administrator',
      'contributeur' => 'contributeur',
    ),
    'module' => 'taxonomy',
  );

  return $permissions;
}
