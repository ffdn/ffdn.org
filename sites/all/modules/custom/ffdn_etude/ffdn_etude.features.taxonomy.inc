<?php

/**
 * @file
 * ffdn_etude.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function ffdn_etude_taxonomy_default_vocabularies() {
  return array(
    'tag_studies' => array(
      'name' => 'Étiquettes (études)',
      'machine_name' => 'tag_studies',
      'description' => 'Utilisez les étiquettes pour regrouper les étudeis sur des sujets similaires dans des catégories.',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
      'language' => 'und',
      'i18n_mode' => 4,
      'base_i18n_mode' => 4,
      'base_language' => 'und',
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
  );
}
