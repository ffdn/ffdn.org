<?php

/**
 * @file
 * ffdn_etude.features.inc
 */

/**
 * Implements hook_views_api().
 */
function ffdn_etude_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function ffdn_etude_node_info() {
  $items = array(
    'etude' => array(
      'name' => t('Étude'),
      'base' => 'node_content',
      'description' => t('Étude de document (SDTAN, etc.)'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
