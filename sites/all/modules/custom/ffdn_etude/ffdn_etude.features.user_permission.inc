<?php

/**
 * @file
 * ffdn_etude.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function ffdn_etude_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create etude content'.
  $permissions['create etude content'] = array(
    'name' => 'create etude content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete any etude content'.
  $permissions['delete any etude content'] = array(
    'name' => 'delete any etude content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete own etude content'.
  $permissions['delete own etude content'] = array(
    'name' => 'delete own etude content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete terms in tag_studies'.
  $permissions['delete terms in tag_studies'] = array(
    'name' => 'delete terms in tag_studies',
    'roles' => array(),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit any etude content'.
  $permissions['edit any etude content'] = array(
    'name' => 'edit any etude content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit own etude content'.
  $permissions['edit own etude content'] = array(
    'name' => 'edit own etude content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit terms in tag_studies'.
  $permissions['edit terms in tag_studies'] = array(
    'name' => 'edit terms in tag_studies',
    'roles' => array(),
    'module' => 'taxonomy',
  );

  return $permissions;
}
