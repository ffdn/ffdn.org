<?php

/**
 * @file
 * ffdn_common.features.contact_categories.inc
 */

/**
 * Implements hook_contact_categories_defaults().
 */
function ffdn_common_contact_categories_defaults() {
  return array(
    'Contact' => array(
      'category' => 'Contact',
      'recipients' => 'contact@ffdn.org',
      'reply' => '',
      'weight' => 0,
      'selected' => 1,
    ),
  );
}
