<?php

/**
 * @file
 * ffdn_common.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function ffdn_common_user_default_roles() {
  $roles = array();

  // Exported role: administrator.
  $roles['administrator'] = array(
    'name' => 'administrator',
    'weight' => 3,
  );

  // Exported role: contributeur.
  $roles['contributeur'] = array(
    'name' => 'contributeur',
    'weight' => 2,
  );

  return $roles;
}
