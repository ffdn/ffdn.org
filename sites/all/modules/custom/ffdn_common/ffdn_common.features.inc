<?php

/**
 * @file
 * ffdn_common.features.inc
 */

/**
 * Implements hook_node_info().
 */
function ffdn_common_node_info() {
  $items = array(
    'page' => array(
      'name' => t('Page de base'),
      'base' => 'node_content',
      'description' => t('Utilisez les <em>pages de base</em> pour votre contenu statique, tel que la page \'Qui sommes-nous\'.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
