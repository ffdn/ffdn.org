/**
 * @file
 * Positioning for responsive layout .
 *
 * Define CSS classes to create a table-free, 3-column, 2-column, or single
 * column layout depending on whether blocks are enabled in the left or right
 * columns.
 *
 * This layout uses the Zen Grids plugin for Compass: http://zengrids.com
 */

/*
 * Center the page.
 */

#header,
#main,
.footer-content,
#main-menu .links {
  /* For screen sizes larger than 1200px, prevent excessively long lines of text
     by setting a max-width. */
  margin-left: auto;
  margin-right: auto;
  min-width: 800px;
  max-width: 1200px;
}

/*
 * Apply the shared properties of grid items in a single, efficient ruleset.
 */

#header,
#content,
#navigation,
.region-sidebar-first,
.region-sidebar-second,
#footer {
  padding-left: 10px;
  padding-right: 10px;
  -moz-box-sizing: border-box;
  -webkit-box-sizing: border-box;
  -ms-box-sizing: border-box;
  box-sizing: border-box;
  word-wrap: break-word;
  _display: inline;
  _overflow: hidden;
  _overflow-y: visible;
}

/*
 * Containers for grid items and flow items.
 */

#header,
#main,
#footer {
  *position: relative;
  *zoom: 1;
}
#header:before, #header:after,
#main:before, #main:after,
#footer:before, #footer:after {
  content: "";
  display: table;
}
#header:after,
#main:after,
#footer:after {
  clear: both;
}

/*
 * Navigation bar
 */

@media all and (min-width: 480px) {
  #header {
    /*min-height: 130px; */
  }

  #navigation {
    /*position: absolute;*/
    /*top: 0; [> Move the navbar up inside #main's padding. <]*/
    /*height: 2em;*/
    width: 100%;
  }

  /*
   * Main menu and Secondary menu links
   */
  #main-menu {
    position: absolute;
    /*top: 130px; */
    left: 0;
    width: 100%
  }
  
  #secondary-menu {
    position: absolute;
    top: 0;
    left: 0;
    margin: 0;
    width: 100%;
  }

  #content,
  #main .sidebars {
    position: relative;
    top: 40px;
    margin-bottom: 50px;
  }

}

/*
 * Use 3 grid columns for smaller screens.
 */

@media all and (min-width: 480px) and (max-width: 959px) {
  /*
   * The layout when there is only one sidebar, the right one.
   */
  .sidebar-second #content { /* Span 2 columns, starting in 1st column from left. */
    float: left;
    width: 66.667%;
    margin-left: 0%;
    margin-right: -66.667%;
  }
  .sidebar-second .region-sidebar-second { /* Span 1 column, starting in 3rd column from left. */
    float: left;
    width: 33.333%;
    margin-left: 66.667%;
    margin-right: -100%;
  }
}

/*
 * Use 5 grid columns for larger screens.
 */

@media all and (min-width: 960px) {

  /*
   * The layout when there is only one sidebar, the right one.
   */

  .sidebar-second #content { /* Span 4 columns, starting in 1st column from left. */
    float: left;
    width: 60%;
    margin-left: 0%;
    margin-right: -60%;
  }
  .sidebar-second .region-sidebar-second { /* Span 1 column, starting in 5th column from left. */
    float: left;
    width: 40%;
    margin-left: 60%;
    margin-right: -100%;
  }
}
